package srp

import (
	"encoding/hex"
	"log"
	"testing"
)

func TestCom(t *testing.T) {
	ch := make(chan []byte)
	done := make(chan bool)

	go runServer(ch, done)
	go runClient(ch, done)

	<-done
	<-done
}

func runServer(ch chan []byte, done chan bool) {
	p, _ := hex.DecodeString("3d0d99423e31fcc67a6745ec89d70d700344bc76")

	// recieve user identifier from client
	I := <-ch

	s, v, _ := Verifier(p, 32)
	server, _ := NewServer(I, v, s, 32)

	// send N, g, s, B
	ch <- server.N.Bytes()
	ch <- server.g.Bytes()
	ch <- s
	ch <- server.B.Bytes()

	// recieve A and M1
	A := <-ch
	M1 := <-ch

	// verify M1
	M2, err := server.ClientOK(A, M1)
	if err != nil {
		log.Fatal(err)
	}

	// send M1
	ch <- M2

	done <- true
}

func runClient(ch chan []byte, done chan bool) {
	I := []byte("test")
	P := []byte("test")

	// send credentials
	client, _ := NewClient(I, P, 32)
	ch <- client.Credentials()

	// recieve N, g, s, B
	N := <-ch
	g := <-ch
	s := <-ch
	B := <-ch

	// generate rest of values and send A and M1
	_, _ = client.Generate(N, g, s, B)
	Ax, _ := hex.DecodeString("7785D59589E0F52230DCCDF22327808617D652ECE5D5CE79060CD0D908E9F630")
	M1x, _ := hex.DecodeString("013466A9026768CB324634A3156BF8344AA7DBB1")

	ch <- Ax
	ch <- M1x

	// recieve and verify M2
	M2 := <-ch
	if !client.ServerOK(M2) {
		log.Fatal("server not ok")
	}

	done <- true
}
