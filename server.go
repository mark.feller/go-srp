package srp

import (
	"fmt"
	"math/big"
)

type Server struct {
	g *big.Int // generator mod N
	N *big.Int // large prime number
	I []byte   // raw credentials
	i []byte   // encrypted credentials
	s []byte   // salt
	v *big.Int // verifier
	b *big.Int // randomly generated number
	B *big.Int // B = kv + g^b % N
	A *big.Int // client public key

	S []byte // unhashed session key
	K []byte // hashed session key
	M []byte // verifier
}

// NewServer creates a new SRP-6 server using credentials I, verifier v, salt s,
// with bytes representing the length in bytes N should be.
func NewServer(I, v, s []byte, bytes int) (c *Server, err error) {
	c = &Server{}
	err = c.init(I, v, s, bytes)

	return
}

func (c *Server) init(I, v, s []byte, bytes int) (err error) {
	pf, ok := pflist[bytes]
	if !ok {
		err = fmt.Errorf("Invalid bits: %d", bytes)
		return
	}

	c.v = big.NewInt(0).SetBytes(v)

	c.b = big.NewInt(0).SetBytes(randbytes(32))
	// bx, _ := hex.DecodeString("3EF0CD48DE65224A5487F27C72A51CA99295A2")
	// c.b = big.NewInt(0).SetBytes(bx)
	c.N = pf.N
	c.g = pf.g
	c.s = make([]byte, len(s))
	copy(c.s, s)
	c.i = hashbyte(I) // wire format of I
	c.I = make([]byte, len(I))
	copy(c.I, I)

	// k = 3 for legacy SRP-6
	k := big.NewInt(3)

	// B = kv + g^b
	t0 := big.NewInt(0).Mul(k, c.v)
	t1 := big.NewInt(0).Add(t0, big.NewInt(0).Exp(c.g, c.b, c.N))
	c.B = big.NewInt(0).Mod(t1, c.N)

	return
}

// Credentials returns N,g,s, and B as per optimal message ordering of SRP-6
//
// return all values as big endian byte slices
func (c *Server) Credentials() (N, g, s, B []byte) {
	return c.N.Bytes(), c.g.Bytes(), c.s, c.B.Bytes()
}

// Generate computes the session key and hashed key after it recieved A from the
// client
func (c *Server) generate(A *big.Int) (err error) {
	c.A = A

	// B should be reversed
	// Hashes should use little endian wire format
	hab := hashbyte(reverseBytes(c.A.Bytes(), 32), reverseBytes(c.B.Bytes(), 32))

	habb := reverseBytes(hab, 20)     // H(A, B) in big endian format
	u := big.NewInt(0).SetBytes(habb) // big endian
	if u.Cmp(big.NewInt(0)) == 0 {
		err = fmt.Errorf("invalid server public key u")
		return
	}

	// session key = (Av^u) ^ b
	t0 := big.NewInt(0).Mul(A, big.NewInt(0).Exp(c.v, u, c.N))
	Sn := big.NewInt(0).Exp(t0, c.b, c.N)
	c.S = Sn.Bytes() // Bytes return big endian format

	// hashed session key
	c.K = zipperHash(c.S) // returns bytes in big endian format

	// verifier
	// need to work out this messed up logic
	c.M = _hmac(c.N.Bytes(), c.g.Bytes(), c.I, c.s, A.Bytes(), c.B.Bytes(), reverseBytes(c.K, 40))

	return
}

// ClientOK checks to see if the client generated the same key using the hash
//
// M = H(H(N) xor H(g), H(I), s, A, B, K)
func (c *Server) ClientOK(A, m1 []byte) ([]byte, error) {
	// Creating a number should use big endian reverse wire format
	An := big.NewInt(0).SetBytes(A)
	c.generate(An)

	M1 := big.NewInt(0).SetBytes(m1)
	M := big.NewInt(0).SetBytes(c.M)

	if M1.Cmp(M) != 0 {
		return nil, fmt.Errorf("client failed to generate same password")
	}
	m2 := reverseBytes(hashbyte(reverseBytes(A, 32), c.M, reverseBytes(c.K, 40)), 20)

	return m2, nil
}

// Generate a password veririer for user I and passphrase p
// Return tuple containing hashed identity, salt, verifier. Caller
// is expected to store the tuple in some persistent DB
//
// p is expected to be H(username, ":", password)
func Verifier(p []byte, bits int) (s, v []byte, err error) {
	pf, ok := pflist[bits]
	if 0 == big.NewInt(0).Cmp(pf.N) || !ok {
		err = fmt.Errorf("invalid bits: %d", bits)
		return
	}

	// x = H(s, p)
	// v = g^x
	s = randbytes(32)
	hps := hashbyte(reverseBytes(s, 32), p)
	rhps := reverseBytes(hps, 20)

	x := big.NewInt(0).SetBytes(rhps)
	v = big.NewInt(0).Exp(pf.g, x, pf.N).Bytes()

	return
}

func (c *Server) String() string {
	str := `
SERVER
c.N         : %x
c.g         : %v
c.I         : %x
c.v         : %v
c.A         : %v
c.A.Bytes() : %x
c.B         : %x
c.K         : %x
c.S         : %x
c.b         : %v
SERVER
`

	return fmt.Sprintf(str, c.N.Bytes(), c.g, c.I, c.v, c.A, c.A.Bytes(), c.B.Bytes(), c.K, c.S, c.b)
}

/// Pakcets on wire need to be reversed
