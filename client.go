package srp

import (
	"fmt"
	"math/big"
	"strings"
)

type Client struct {
	g *big.Int // generator mod N
	N *big.Int // large prime number
	I []byte   // raw credentials
	i []byte   // encrypted credentials
	p []byte   // raw password
	s []byte   // user salt
	a *big.Int // randomly generated number
	A *big.Int // A = g^a % N
	B *big.Int // server public key
	k *big.Int // k = H(N, g) in SRP-6a, k = 3 for legacy SRP-6

	S []byte // unhashed session key
	K []byte // hashed session key
	M []byte // verifier
}

// NewClient initializes the connection where I is the raw credentials p is the raw
// password and bits in the number of bytes used in the salt
func NewClient(I, p []byte, bytes int) (c *Client, err error) {
	pf, ok := pflist[bytes]
	if 0 == big.NewInt(0).Cmp(pf.N) || !ok {
		err = fmt.Errorf("Invalid bits: %d", bytes)
		return
	}

	c = &Client{
		i: hashbyte(I),
		I: I,
		p: p,
		k: big.NewInt(3), // k = 3 for legacy SRP-6
	}

	return
}

// Credentials returns the encrypted credentials that are sent to the server
func (c *Client) Credentials() []byte {
	return c.I
}

// Generate calculate public key A and session keys.
func (c *Client) Generate(N, g, s, Bs []byte) (A []byte, M []byte) {
	c.N = big.NewInt(0).SetBytes(N)
	c.g = big.NewInt(0).SetBytes(g)
	c.s = s
	c.B = big.NewInt(0).SetBytes(Bs)
	c.k = big.NewInt(3)

	c.a = randlong(19)
	c.A = big.NewInt(0).Exp(c.g, c.a, c.N)

	zero := big.NewInt(0)
	z := big.NewInt(0).Mod(c.B, c.N)
	if zero.Cmp(z) == 0 {
		return
	}

	// u = H(A, B)
	u := hashint(reverseIntBytes(c.A, 32), reverseIntBytes(c.B, 32))
	if u.Cmp(zero) == 0 {
		return
	}

	// x = H(s, H(I:p))
	userStr := strings.ToUpper(string(c.I) + ":" + string(c.p))
	p := hashbyte([]byte(userStr))
	// fmt.Println(c.s)
	rs := reverseBytes(c.s, 32)
	x := big.NewInt(0).SetBytes(hashbyte(rs, p))

	// fmt.Println(x)

	// S = (B - k.g^x) ^ (a + u.x)
	t0 := big.NewInt(0).Exp(c.g, x, c.N)                  // t0 = g^x
	t1 := t0.Mul(t0, c.k)                                 // t1 = k.g^x
	t2 := big.NewInt(0).Sub(c.B, t1)                      // t2 = B - k.g^x
	t3 := big.NewInt(0).Add(c.a, big.NewInt(0).Mul(u, x)) // t3 = a + u.x
	c.S = big.NewInt(0).Exp(t2, t3, c.N).Bytes()          // S = (B - k.g^x) ^ (a + u.x)

	c.K = zipperHash(c.S)

	// // debug
	// fmt.Println(c)

	// M = H( H(N) ^ H(g), H(I), s, A, B, K)
	c.M = _hmac(c.N.Bytes(), c.g.Bytes(), c.I, s, c.A.Bytes(), c.B.Bytes(), c.K)

	return c.A.Bytes(), c.M
}

func (c *Client) ServerOK(m []byte) bool {
	// H(A, M, K)
	mym := hashbyte(reverseIntBytes(c.A, 32), c.M, reverseBytes(c.K, 40))

	return byteeq(mym, m)
}

func (c *Client) String() string {
	str := `
CLIENT
c.N.Bytes() : %x
c.g.Bytes() : %x
c.I         : %x
c.s         : %x
A.Bytes()   : %x
c.B.Bytes() : %x
c.K         : %x
c.S         : %x
CLIENT
`

	return fmt.Sprintf(str, c.N.Bytes(), c.g.Bytes(), c.I, c.s, c.A.Bytes(), c.B.Bytes(), c.K, c.S)

}
