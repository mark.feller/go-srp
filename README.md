# Standalone SRP-6a implementation in go-lang


This is a standalone implementation of SRP-6 in golang. It uses the go
standard libraries and has no other external dependencies. This
library can be used by SRP clients or servers.

SRP is a protocol to authenticate a user and derive safe session
keys. It is the latest in the category of "strong authentication
protocols".

This uses the optimized messege ordering

SRP is documented here: http://srp.stanford.edu/doc.html

