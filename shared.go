package srp

import (
	"crypto/rand"
	"crypto/sha1"
	"fmt"
	"io"
	"math/big"
	"strings"
)

// wow only uses 32 Byte N
var pflist_str = map[int][2]string{
	32: {"7", "0x894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7"},
}

type prime_field struct {
	g *big.Int // generator mod n
	N *big.Int // n large prime number
}

// prime field list - mapped by bit size
var pflist map[int]prime_field

// hashing algorithm used
var mac = sha1.New

// build the database of prime fields and generators
func init() {
	pflist = make(map[int]prime_field)

	for bits, arr := range pflist_str {
		g, ok0 := big.NewInt(0).SetString(arr[0], 10)
		n, ok1 := big.NewInt(0).SetString(arr[1], 0)

		if !ok0 {
			s := fmt.Sprintf("srp init: Can't parse string %s", arr[0])
			panic(s)
		}

		if !ok1 {
			s := fmt.Sprintf("srp init: Can't parse string %s", arr[1])
			panic(s)
		}

		pflist[bits] = prime_field{g: g, N: n}
	}
}

func randlong(bits int) *big.Int {
	n := bits / 8
	if 0 == bits%8 {
		n += 1
	}
	b := randbytes(n)
	r := big.NewInt(0).SetBytes(b)
	return r
}

func randbytes(n int) []byte {
	b := make([]byte, n)
	_, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		panic("Random source is broken!")
	}
	return b
}

// hash byte stream and return as bytes
func hashbyte(a ...[]byte) []byte {
	h := mac()
	for _, z := range a {
		h.Write(z)
	}
	return h.Sum(nil)
}

func safeXORBytes(dst, a, b []byte) int {
	n := len(a)
	if len(b) < n {
		n = len(b)
	}
	for i := 0; i < n; i++ {
		dst[i] = a[i] ^ b[i]
	}
	return n
}

func _hmac(N, g, I, s, A, B, vK []byte) []byte {
	hN := hashbyte(reverseBytes(N, 32)) // little endian format
	hg := hashbyte(reverseBytes(g, 1))  // little endian format
	hash := make([]byte, 20)
	safeXORBytes(hash, hN, hg)

	K := big.NewInt(0).SetBytes(vK) // vK is in big endian format

	Iu := []byte(strings.ToUpper(string(I)))
	userHash := hashbyte(Iu)
	// t4 := setbinary(userHash, true)
	t4 := big.NewInt(0).SetBytes(userHash)

	Bl := reverseBytes(B, 32) // B in little endian format
	Al := reverseBytes(A, 32) // A in little endian format
	Kb := K.Bytes()           // k in big endian format

	return hashbyte(hash, t4.Bytes(), s, Al, Bl, Kb)
}

func reverse(s []byte) []byte {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

func zipperHash(S []byte) []byte {
	t := make([]byte, 32)
	t1 := make([]byte, 16)
	t2 := make([]byte, 16)
	vK := make([]byte, 40)

	// little endian wire format for hash
	t = reverseBytes(S, 32)

	for i := 0; i < 16; i++ {
		t1[i] = t[i*2]
		t2[i] = t[i*2+1]
	}

	ht1 := hashbyte(t1)
	ht2 := hashbyte(t2)

	for i := 0; i < 20; i++ {
		vK[i*2] = ht1[i]
		vK[i*2+1] = ht2[i]
	}

	// vK is little endian, return big endian format
	return reverseBytes(vK, 40)
}

func reverseBytes(b []byte, size int) []byte {
	r := make([]byte, size)
	copy(r, b)

	if b[0] == 0 {
		r = r[1:]
	}

	r = reverse(r)

	if size > len(r) {
		newR := make([]byte, size)
		copy(newR, r[:])
		return newR
	}

	return r

}

// Depricate

func setbinary(array []byte, reverse bool) *big.Int {
	b := make([]byte, len(array))
	copy(b, array)

	if reverse {
		reverseBytes(b, len(b))
	}

	if b[0] < 0 {
		r := make([]byte, len(b)+1)
		for i := 0; i < len(b); i++ {
			r[i+1] = b[i]
		}
		return big.NewInt(0).SetBytes(r)
	}

	return big.NewInt(0).SetBytes(b)
}

func reverseIntBytes(i *big.Int, size int) []byte {
	b := i.Bytes()

	return reverseBytes(b, size)
}

// byte compare
func byteeq(a []byte, b []byte) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

// hash a number of byte strings and return the resulting hash as
// bigint
func hashint(a ...[]byte) *big.Int {
	i := big.NewInt(0)
	b := hashbyte(a...)
	i.SetBytes(b)
	return i
}
