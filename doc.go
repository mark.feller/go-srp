// Copyright 2013-2014 Sudhi Herle <sudhi.herle-at-gmail-dot-com>
// License: MIT

/*
Implementation of SRP_.  It requires a cryptographically strong
random number generator.

This implementation is accurate as of Aug 2012 edition of the SRP_
specification.

Conventions
-----------
  N    A large safe prime (N = 2q+1, where q is prime)
	   All arithmetic is done modulo N.
  g    A generator modulo N
  k    Multiplier parameter (k = H(N, g) in SRP-6a, k = 3 for legacy SRP-6)
  s    User's salt
  I    Username
  p    Cleartext Password
  H()  One-way hash function
  ^    (Modular) Exponentiation
  u    Random scrambling parameter
  a,b  Secret ephemeral values
  A,B  Public ephemeral values
  x    Private key (derived from p and s)
  v    Password verifier

The host stores passwords using the following formula:

  x = H(s, p)               (s is chosen randomly)
  v = g^x                   (computes password verifier)

The host then keeps {I, s, v} in its password database.

The authentication protocol itself goes as follows:

		User -> Host:  I                   (identifies self)
		Host -> User:  N, g, s, B = g^b    (sends salt, b = random number)

		User:  A = g ^ a                   (a = random number)
		User:  u = H(A, B)
		User:  x = H(s, p)                 (user enters password)
		User:  S = (B - kg^x) ^ (a + ux)   (computes session key)
		User:  K = H(S)

		User -> Host:  A, M = H(H(N) xor H(g), H(I), s, A, B, K)

		Host:  u = H(A, B)
		Host:  S = (Av^u) ^ b              (computes session key)
		Host:  K = H(S)

		Host -> User:  H(A, M, K)

Now the two parties have a shared, strong session key K.
To complete authentication, they need to prove to each other that
their keys match.


The two parties also employ the following safeguards:

 1. The user will abort if he receives B == 0 (mod N) or u == 0.
 2. The host will abort if it detects that A == 0 (mod N).
 3. The user must show his proof of K first. If the server detects that the
	user's proof is incorrect, it must abort without showing its own proof of K.

In this implementation::

	H  = SHA1()
	k  = 3
	x  = zipperHash(s, I, P)
	I  = unhashed user identity

This convention guarantees that both parties can mutually conclude
that they have generated an identical key.


.. _SRP: http://srp.stanford.edu/
*/
package srp
